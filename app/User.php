<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class); // el usuario que llama a que rol pertenece.
        //belongsTo -> pertenece a uno -> N:1
        //hasMany -> tiene varios -> 1:N
        //hasOne -> tiene uno -> 1:1
        //belongsToMany (pertenece a varios -> N:M)
    }

    public function citas(){

        return $this->belongsToMany(Cita::class); // una cita involucra a muchos --> Many To Many
        // $pedido->citas->attach($idArticulo, ['cantidad', $cantidad]);
    }
}
