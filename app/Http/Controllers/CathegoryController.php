<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegory;
use App\Product;
use App\User;

class CathegoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth')->except('index'); // aun sin logear podremos ver el listado.
    }

    public function index()
    {
        $categorias = Cathegory::paginate(10);
        return view('cathegory.index', ['categorias' => $categorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currentUser = \Auth::user();
        if ($currentUser->can('create', Cathegory::class)) { // la diferencia entre el authorize y ésto es que el can te redirecciona a la página que quieras, en cambio, el authorize manda a un 403 siempre.
            return view('cathegory.create');
        }else{
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
        ];

        $request->validate($rules);

        $categoria = new Cathegory();
        $categoria->fill($request->all());
        $categoria->save();

        return redirect('/cathegories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = Cathegory::findOrFail($id);
        $productosAsociados = Product::all();
        return view('cathegory.show', [
            'cathegory' => $categoria,
            'productosAsociados' => $productosAsociados,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = Cathegory::findOrFail($id);
        return view('cathegory.edit', [
            'cathegory' => $categoria,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $rules = [
        'name' => 'required|max:255|min:3',

    ];

    $request->validate($rules);

    $categoria = Cathegory::findOrFail($id);
    $categoria->fill($request->all());
    $categoria->save();


    return redirect('/cathegories/' . $categoria->id);
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Cathegory::findOrFail($id);
        if (count($categoria->products) != 0){
            return response()->view('errors.usedCathegory', ['cathegory' => $categoria]);
        }

        Cathegory::destroy($id);
        return back();
    }
}
